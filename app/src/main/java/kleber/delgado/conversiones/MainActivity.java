package kleber.delgado.conversiones;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnf =(Button) findViewById(R.id.btn_f);
        Button btnc =(Button) findViewById(R.id.btn_c);

        final EditText editTextf =(EditText) findViewById(R.id.text_f);
        final EditText editTextc =(EditText) findViewById(R.id.text_c);

        btnf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sfar = editTextf.getText().toString();
                float far = Float.parseFloat(sfar);
                float ce =(far-32) / 1.8000f;
                String sce =String.format("%f",ce);
                editTextc.setText(sce);


            }
        });
        btnc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sce = editTextc.getText().toString();
                float ce = Float.parseFloat(sce);
                float far = ce*1.8000f +32f;
                String sfar = String.format("%f", far);
                editTextf.setText(sfar);
            }
        });
    }
}
